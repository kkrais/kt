#!/bin/bash
#Autor: Karl-Erik Krais
#Skripti kirjeldus: Väljastatakse kõik võimalikud PIN koodid, luuakse kasutaja sisestatud numbritega kaust, sinna sisse fail koodid.txt ja selle sisse koodid koos kasutaja PIN koodiga kuni 9999 

export LC_ALL=C
export LANG=C

#PIN=$1
#Kontrollime parameetrite arvu:
size=${#1} 

if [ $# -ne 1 ]; then
    echo "Vale kasutus! Kasutamine $0 1234"
    exit 1
fi

if [ $size -ne 4 ]; then
    echo "Vale kasutus! Kasutamine $0 1234"
    exit 1
fi
#Kontrollime, kas PIN kood koosneb ainult numbritest:

if ! [[ "${1:$((1)):4}" =~ [^0-9] ]];
then 
    PIN=$1
else
    echo "Viga sisestuses, valed sümbolid. Kasuta ainult numbreid!"
    exit 1
fi

function nmbr {
START=0000
END=9999
NUMBER=1

i=$START

#Kontrollime, kas kaust eksisteerib, vajadusel loome:
if [ ! -d "$PIN" ]
    then mkdir -p $PIN 
fi
#Kas kausta loomine õnnestus:
    if [ $? -ne 0 ]; then
        echo "Kausta loomisel tekkis viga"
        exit 2
    fi

while [[ $i -le $END ]]
do    
    echo $i
    if [ $i -eq $PIN ] #Kontrollime ,kas praegune number on võrdne sisendisse antud numbriga
        then 
        if [ -f "$PIN"/koodid.txt ]; #Vaatame ,kas fail nimega koodid.txt eksisteerib
            then
            while [[ -e "$PIN"/koodid$NUMBER.txt ]] ; #Kui eksisteerib, lisab nimesse numbri
                do
                    let NUMBER++ 
            done
            
            FILE="$PIN"/koodid$NUMBER.txt #Uue faili nimi, millele on lisatud number
            
            else
            
                FILE="$PIN"/koodid.txt #Vastasel juhul jätab nime samaks
                fi
                touch $FILE #Loome faili
        fi
    if [[ $i -ge $PIN ]] #Kui genereeritud number on suurem-võrdne sisendiga, siis lisatase(print) see number faii lõppu
        then printf "%04d\n" $i >> $FILE #Numbrid 4-kohaliseks
        fi
    ((i=i + 1))
done

}


nmbr $# #Kutsub funktsiooni väja







